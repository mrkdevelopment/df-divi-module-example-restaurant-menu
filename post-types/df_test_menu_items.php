<?php

function df_test_menu_items_init() {
	register_post_type( 'df_test_menu_items', array(
		'labels'            => array(
			'name'                => __( 'Restaurant Menu - Tests', 'df-divi-module-example-restaurant-menu' ),
			'singular_name'       => __( 'Restaurant Menu - Test', 'df-divi-module-example-restaurant-menu' ),
			'all_items'           => __( 'All Restaurant Menu - Tests', 'df-divi-module-example-restaurant-menu' ),
			'new_item'            => __( 'New Restaurant Menu - Test', 'df-divi-module-example-restaurant-menu' ),
			'add_new'             => __( 'Add New', 'df-divi-module-example-restaurant-menu' ),
			'add_new_item'        => __( 'Add New Restaurant Menu - Test', 'df-divi-module-example-restaurant-menu' ),
			'edit_item'           => __( 'Edit Restaurant Menu - Test', 'df-divi-module-example-restaurant-menu' ),
			'view_item'           => __( 'View Restaurant Menu - Test', 'df-divi-module-example-restaurant-menu' ),
			'search_items'        => __( 'Search Restaurant Menu - Tests', 'df-divi-module-example-restaurant-menu' ),
			'not_found'           => __( 'No Restaurant Menu - Tests found', 'df-divi-module-example-restaurant-menu' ),
			'not_found_in_trash'  => __( 'No Restaurant Menu - Tests found in trash', 'df-divi-module-example-restaurant-menu' ),
			'parent_item_colon'   => __( 'Parent Restaurant Menu - Test', 'df-divi-module-example-restaurant-menu' ),
			'menu_name'           => __( 'Restaurant Menu - Tests', 'df-divi-module-example-restaurant-menu' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-admin-post',
		'show_in_rest'      => true,
		'rest_base'         => 'df_test_menu_items',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'df_test_menu_items_init' );

function df_test_menu_items_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['df_test_menu_items'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Restaurant Menu - Test updated. <a target="_blank" href="%s">View Restaurant Menu - Test</a>', 'df-divi-module-example-restaurant-menu'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'df-divi-module-example-restaurant-menu'),
		3 => __('Custom field deleted.', 'df-divi-module-example-restaurant-menu'),
		4 => __('Restaurant Menu - Test updated.', 'df-divi-module-example-restaurant-menu'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Restaurant Menu - Test restored to revision from %s', 'df-divi-module-example-restaurant-menu'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Restaurant Menu - Test published. <a href="%s">View Restaurant Menu - Test</a>', 'df-divi-module-example-restaurant-menu'), esc_url( $permalink ) ),
		7 => __('Restaurant Menu - Test saved.', 'df-divi-module-example-restaurant-menu'),
		8 => sprintf( __('Restaurant Menu - Test submitted. <a target="_blank" href="%s">Preview Restaurant Menu - Test</a>', 'df-divi-module-example-restaurant-menu'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Restaurant Menu - Test scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Restaurant Menu - Test</a>', 'df-divi-module-example-restaurant-menu'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Restaurant Menu - Test draft updated. <a target="_blank" href="%s">Preview Restaurant Menu - Test</a>', 'df-divi-module-example-restaurant-menu'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'df_test_menu_items_updated_messages' );
