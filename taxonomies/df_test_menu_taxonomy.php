<?php

function df_test_menu_taxonomy_init() {
	register_taxonomy( 'df_test_menu_taxonomy', array( 'df_test_menu_items' ), array(
		'hierarchical'      => false,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => false,
		'query_var'         => true,
		'rewrite'           => true,
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts'
		),
		'labels'            => array(
			'name'                       => __( 'Menu Categories', 'df-divi-module-example-restaurant-menu' ),
			'singular_name'              => _x( 'Menu Category', 'taxonomy general name', 'df-divi-module-example-restaurant-menu' ),
			'search_items'               => __( 'Search Menu Categories', 'df-divi-module-example-restaurant-menu' ),
			'popular_items'              => __( 'Popular Menu Categories', 'df-divi-module-example-restaurant-menu' ),
			'all_items'                  => __( 'All Menu Categories', 'df-divi-module-example-restaurant-menu' ),
			'parent_item'                => __( 'Parent Menu Category', 'df-divi-module-example-restaurant-menu' ),
			'parent_item_colon'          => __( 'Parent Menu Category:', 'df-divi-module-example-restaurant-menu' ),
			'edit_item'                  => __( 'Edit Menu Category', 'df-divi-module-example-restaurant-menu' ),
			'update_item'                => __( 'Update Menu Category', 'df-divi-module-example-restaurant-menu' ),
			'add_new_item'               => __( 'New Menu Category', 'df-divi-module-example-restaurant-menu' ),
			'new_item_name'              => __( 'New Menu Category', 'df-divi-module-example-restaurant-menu' ),
			'separate_items_with_commas' => __( 'Separate Menu Categories with commas', 'df-divi-module-example-restaurant-menu' ),
			'add_or_remove_items'        => __( 'Add or remove Menu Categories', 'df-divi-module-example-restaurant-menu' ),
			'choose_from_most_used'      => __( 'Choose from the most used Menu Categories', 'df-divi-module-example-restaurant-menu' ),
			'not_found'                  => __( 'No Menu Categories found.', 'df-divi-module-example-restaurant-menu' ),
			'menu_name'                  => __( 'Menu Categories', 'df-divi-module-example-restaurant-menu' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'df_test_menu_taxonomy',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'df_test_menu_taxonomy_init' );
