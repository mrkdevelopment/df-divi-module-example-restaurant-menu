<?php
namespace DF\ExampleRestaurantMenu;

/**
 * Register divi modules
 */
class DiviModules
{
    
    protected $container;


    public function __construct($container)
    {
        $this->container = $container;
    }



    /**
     * Register divi modules.
     */
    public function register()
    {
        // main module.
        new TabbedMenuModule($this->container);

        // register child menu items
        new MenuItemsModule($this->container);
    }
}
