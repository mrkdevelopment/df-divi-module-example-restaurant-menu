<?php
namespace DF\ExampleRestaurantMenu;

use ET_Builder_Element;
use ET_Builder_Module;

/**
 *
 */
class MenuItemsModule extends ET_Builder_Module
{
    public $name = 'Restaurant Menu Items';
    public $slug = 'df_example_menu_items';
    public $fields;
    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
        $this->initFields();
        parent::__construct();
    }

    /**
     * Initialise the fields.
     */
    private function initFields()
    {
        $this->fields = array();

        $this->fields['title'] = array(
                'label'       => esc_html__('Title', 'et_builder'),
                'type'        => 'text',
                'description' => esc_html__('The title will be used within the tab button for this tab.', 'et_builder'),
            );

        $this->fields['include_categories']   = array(
            'label'            => esc_html__('Include Categories', 'et_builder'),
            'type'             => 'multiple_checkboxes',
            'renderer'         => 'et_builder_include_categories_option',
            'option_category'  => 'basic_option',
            'renderer_options' => array(
                'use_terms'    => true,
                'term_name'    => 'df_test_menu_taxonomy',
            ),
            'description'      => esc_html__('Choose which categories you would like to include.', 'et_builder'),
            'taxonomy_name'    => 'df_test_menu_taxonomy',
        );
    }

    /**
     * Init module.
     */
    public function init()
    {
        $this->whitelisted_fields = array_keys($this->fields);

        $this->type  = 'child';
        $this->child_title_var  = 'title';

        $this->whitelisted_fields = array(
            'title',
        );


        $defaults = array();

        $this->field_defaults = $defaults;

        $this->main_css_element = '%%order_class%%';
    }

    /**
     * Get Fields
     *
     */
    public function get_fields()
    {
        return $this->fields;
    }

    /**
     * Shortcode render.
     */
    public function shortcode_callback($atts, $content = null, $function_name)
    {
        $module_class = $this->shortcode_atts['module_class'];
        $module_class = ET_Builder_Element::add_module_order_class($module_class, $function_name);
        $atts['selector'] = '.' . trim($module_class);
        $atts['module_class']  = trim($module_class);

        // return back a string.
        return $module_class;
    }
}
