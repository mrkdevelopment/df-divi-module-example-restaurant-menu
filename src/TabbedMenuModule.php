<?php
namespace DF\ExampleRestaurantMenu;

use ET_Builder_Element;
use ET_Builder_Module;

/**
 *
 */
class TabbedMenuModule extends ET_Builder_Module
{
    public $name = 'Restaurant Tabbed Menu - Test';
    public $slug = 'et_pb_df_example_restaurant_tabbed_menu';
    public $fields;
    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
        $this->initFields();
        parent::__construct();
    }

    /**
     * Initialise the fields.
     */
    private function initFields()
    {
        $this->fields = array();

        
        $this->fields['admin_label'] = array(
            'label' => __('Admin Label', 'et_builder'),
            'type' => 'text',
            'description' => __('This will change the label of the module in the builder for easy identification.', 'et_builder'),
        );
    }

    /**
     * Init module.
     */
    public function init()
    {
        $this->whitelisted_fields = array_keys($this->fields);

        $this->fb_support      = false;
        $this->child_slug      = 'df_example_menu_items';
        $this->child_item_text = esc_html__('Menu Items', 'et_builder');

        $defaults = array();

        
        $this->field_defaults = $defaults;

        $this->main_css_element = '%%order_class%%';
    }

    
   /**
     * Get Fields
     *
     */
    public function get_fields()
    {
        return $this->fields;
    }

    /**
     * Shortcode render.
     */
    public function shortcode_callback($atts, $content = null, $function_name)
    {
        return $this->shortcode_content;
    }
}
