<?php

namespace DF\ExampleRestaurantMenu;

/**
 * Class defines custom post types.
 */
class CustomPosts
{
    
    protected $container;

    private $label, $args;

    public function __construct($container)
    {
        $this->container = $container;
    }


    // register your custom post and taxonomy here.
    public function register()
    {
        $postsDir = $this->container['plugin_dir'] . '/post-types';
        $taxonomiesDir = $this->container['plugin_dir'] . '/taxonomies';

        include_once $postsDir . '/df_test_menu_items.php';
        include_once $taxonomiesDir . '/df_test_menu_taxonomy.php';
    }
}
