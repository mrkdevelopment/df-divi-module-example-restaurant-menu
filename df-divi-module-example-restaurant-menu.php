<?php
/**
 * Plugin Name:     Example Restaurant Menu
 * Plugin URI:      https://www.diviframework.com
 * Description:     Example Restaurant Menu
 * Author:          Divi Framework
 * Author URI:      https://www.diviframework.com
 * Text Domain:     df-divi-module-example-restaurant-menu
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package
 */

if (! defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

define('DF_DIVI_EXAMPLE_MODULE_VERSION', '0.1.0');
define('DF_DIVI_EXAMPLE_MODULE_DIR', __DIR__);
define('DF_DIVI_EXAMPLE_MODULE_URL', plugins_url('/'.basename(__DIR__)));

require_once DF_DIVI_EXAMPLE_MODULE_DIR . '/vendor/autoload.php';

$container = new \DF\ExampleRestaurantMenu\Container;
$container['plugin_name'] = 'Example Restaurant Menu';
$container['plugin_version'] = DF_DIVI_EXAMPLE_MODULE_VERSION;
$container['plugin_file'] = __FILE__;
$container['plugin_dir'] = DF_DIVI_EXAMPLE_MODULE_DIR;
$container['plugin_url'] = DF_DIVI_EXAMPLE_MODULE_URL;
$container['plugin_slug'] = 'df-divi-module-example-restaurant-menu';

// activation hook.
register_activation_hook(__FILE__, array($container['activation'], 'install'));

$container->run();
